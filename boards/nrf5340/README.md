# The N53 Board

![Board Image](nrf5340.png)

The N53 offers complete solution for the Nordic nRF5340 MCU.

Features include:

* Nordic nRF5340 Dual Core, Bluetooth/Thread/Zigbee MCU
* 1.8-3.6V power input
* On-chip DC-DC buck converters for both Cortex-M33 cores
* 32kHz crystal for lowest power consumption
* USB support (can't be used as power supply)
* NFC tag support
* Ultra-miniature built-in 2.45GHz chip antenna
* 64 Mbit (8x8) high speed serial NOR flash memory
* 25 GPIO ports, 7 of which analog inputs
* On-board hardware reset pad
* On-board RGB status LED

## Board Specification

Capability              | Value
------------------------|------------------
Board size              | standard (20x20 mm)
Stack connector         | yes
Mating height           | 1.5 mm
PCB thickness           | 1.0 mm
PCB subtrate            | Rogers R4350B or similar
PCB stackup             | 2+2+2 HDI, vias-in-pad, stacked u-vias, no buried
Finished copper         | 1 oz/sq.ft
Min track / spacing     | 4 mil = 0.1 mm
Min via hole drill size | 10 mil = 0.26 mm / 4 mil = 0.1 mm u-via
Min via annular ring    | 5 mil = 0.125 mm / 4 mil = 0.1 mm u-via
