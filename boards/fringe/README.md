# rayStack Fringe

![Board Image](fringe-3d.png)

**WARNING: THE BOARD IS OBSOLETE AND MUST BE REDESIGNED**

The Fringe is a bread-board adapter for rayStack boards.  It offers
power supply, lithium bttery charger, two buttons, one LED, as well
as USB and 10-pin Cortex SWD ports in a friendly Feather-compatible
package.

## Board Specification

Capability              | Value
------------------------|------------------
Board size              | 22.9 x 50.8
Stack connector         | only front
Mating height           | 3.5 mm
PCB substrate           | FR-4
PCB thickness           | 1.6 mm
Layers                  | 2 layers
Finished copper         | 1 oz/sq.ft
Min track / spacing     | 6 mil = 0.16 mm
Min via hole drill size | 12 mil = 0.3 mm
Min via annular ring    | 6 mil = 0.6 mm
