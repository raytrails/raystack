# The rayStack Platform

rayStack is a highly modular wearable platform. It's optimized for
space-constrained applications where size should not compromise
processing power.

![rayStack](photos/s-raystack-1c.jpg)

## Mechanical Features

* TinyDuino compatible 20x20 mm outline and mounting - 4xM1.2 (requires
  adaptor to connect TinyDuino modules)
* Double sided Hirose DF40 40-pin mezzanine connector
* Variable mating height from 1.5 mm to 4.0 mm.

## Modules

* [N53](boards/nrf5340/) - nRF5340 module with 8MB flash memory and built-in antenna
* [thirtytwo](boards/thirtytwo/) - nRF52832 module with built-in antenna

## Development

The platform is under development. For news please follow
[@mishkathebear](https://twitter.com/mishkathebear) on Twitter.
